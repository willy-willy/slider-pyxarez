(function() {
	class Slider {

		constructor(name) {
			this.el = document.getElementsByClassName(name)[0],
			this.init()
		} 

		init() {
			this.row = this.el.getElementsByClassName('carousel__row')[0],
			this.row.style.left = "0px",
			this.arrows = this.el.getElementsByClassName('arrow'),
			this.position = 1,
			this.maxPositions = this.row.querySelectorAll('img').length / 3,	
			this.makeListener(this.arrows);
		}

		makeListener(arrows, position) {
			let that = this;

			arrows[1].addEventListener('click', (e) => {
				if (that.position !== that.maxPositions) {
					let tempLeft = parseInt(that.row.style.left); 
					that.row.style.left = tempLeft - 930 + "px";
					that.position++;
				}

				if (that.position !== 0 ) {
					arrows[0].classList.remove('blocked');
				}

				if (that.position === that.maxPositions) {
					arrows[1].classList.add('blocked');
				}
			});

			arrows[0].addEventListener('click', (e) => {
				if (that.position !== 1) {
					let tempLeft = parseInt(that.row.style.left); 
					that.row.style.left = tempLeft + 930 + "px";
					that.position--;
				}

				if (that.position !== that.maxPositions ) {
					arrows[1].classList.remove('blocked');
				}

				if (that.position === 1) {
					arrows[0].classList.add('blocked');
				}
			});		
		}
	};
	
	let aSlider = new Slider("container");
})();


